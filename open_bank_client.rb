# Easy access to response data
class RestClient::Response
  def data
    if body && body.is_a?(String) && !body.strip.empty?
      return JSON.parse(body)
    end
  end
end

RestClient.log = 'stdout'

class OpenBankClient

  attr_reader :username, :password, :consumer_key, :auth_url, :api_base_url, :auth_token

  def initialize(username:, password:, consumer_key:, auth_url:, api_base_url:)
    @username = username
    @password = password
    @consumer_key = consumer_key
    @auth_url = auth_url
    @api_base_url = api_base_url

    authenticate!
  end

  def authorized_header
    @auth_token ? { authorization: 'DirectLogin token="%s"' % @auth_token } : {}
  end

  def default_headers(custom_headers)
    headers = {
        content_type: :json,
        accept: :json,
      }

    headers.merge(authorized_header).merge(custom_headers)
  end

  def get(path, **custom_headers)
    RestClient.get(path, default_headers(custom_headers))
  # rescue => e
  #   binding.pry
  end

  def post(path, body = nil, **custom_headers)
    RestClient.post(path, body, default_headers(custom_headers))
  # rescue => e
  #   binding.pry
  end

  def put(path, body = nil, **custom_headers)
    RestClient.put(path, body, default_headers(custom_headers))
  # rescue => e
  #   binding.pry
  end

  # Authenticate
  # POST /my/logins/direct
  # Body
  #   Leave Empty!
  # Headers:
  #   Content-Type:  application/json
  # Authorization: DirectLogin username="janeburel",
  #   password="the-password-of-jane",
  #   consumer_key="your-consumer-key-from-step-one"
  def authenticate!
    headers = {
      authorization: 'DirectLogin username="%s", password="%s", consumer_key="%s"' % [@username, @password, @consumer_key],
    }

    response = post(@auth_url, nil, default_headers(headers))

    @auth_token = response.data["token"]
  end

  def get_banks
    get(@api_base_url + '/banks').data["banks"]
  end

  def get_bank(bank_id)
    get(@api_base_url + '/banks/' + bank_id).data
  end

  def get_bank_atms(bank_id)
    get(@api_base_url + '/banks/' + bank_id + '/atms').data
  end

end
