# README #

This app contains:
* OpenBankClient - connects us to APC's Open Bank Project API
* config.yml - holds some settings needed to connect to the API
* test.rb - gives us an example usages of the client

### How do I get set up? ###

Local development and testing:

* Install `rvm`: https://rvm.io/rvm/install
* Run `rvm install 2.4`
* Clone the repo
* Run `bundle install`
* Run `ruby test.rb`

### Contribution guidelines ###

Always do a `git pull --rebase` before commiting changes.

Currently, endpoints are just being added to `open_bank_client.rb`.

Experiments and examples can be added to `test.rb`.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
