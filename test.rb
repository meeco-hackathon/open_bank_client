require 'rest-client'
require 'json'
require 'pry'
require 'yaml'
require 'pp' # pretty print

require './open_bank_client'

# Login Details
config = YAML.load_file('config.yml')

client_config = {
  username: config['username'],
  password: config['password'],
  consumer_key: config['consumer_key'],
  auth_url: config['direct_login_endpoint'],
  api_base_url: config['api_base_url'],
}

client = OpenBankClient.new(**client_config)

banks = client.get_banks

puts "== Banks =="
pp banks

bank_ids = banks.collect {|b| b['id'] }

puts "== Bank =="
nab = client.get_bank(bank_ids.first)
pp nab

atms = client.get_bank_atms(bank_ids.first)

binding.pry
